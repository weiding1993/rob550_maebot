
class A_star:
	
	def __init__(self,init_node, goal, grid_n_rows, grid_n_cols, obstacle_list):
		
		self.closed_set = []
		self.open_set = [init_node]
		self.path = []
		self.obstacles = obstacle_list;
		self.curr_node = init_node
		self.g_score = 0
		self.init_node = init_node
		self.goal_node = goal
		self.n_rows = grid_n_rows
		self.n_cols = grid_n_cols
		self.obstacle_list = obstacle_list
		self.loopCount = 0


	def get_neighbors(self, curr_node):
		
		dirs = [(1, 0), (0, 1), (-1 ,0), (0,-1), (-1,-1), (-1, 1), (1, 1), (1, -1)]
		# dirs = [(1, 0), (0, 1), (-1 ,0), (0,-1)]
		# 
		neighbors = []
		obstacles = []
		neighbor_num = 0

		for dir in dirs:
			tmp = (curr_node[0] + dir[0], curr_node[1] + dir[1])
			if (0 <= tmp[0] < self.n_rows) and (0 <= tmp[1] < self.n_cols) and (tmp not in self.obstacle_list):
				neighbors.append(tmp)
				neighbor_num += 1
			elif(tmp in self.obstacle_list):
				obstacles.append(tmp)

		return neighbors, neighbor_num, obstacles

	def update_init_node(self, new_init_node):
		self.init_node = new_init_node

	def get_heuristic(self, next_node):
		heuristic = abs(self.goal_node[0] - next_node[0]) + abs(self.goal_node[1] - next_node[1])
		return heuristic

	def search(self):
		while self.open_set:
			print 'while'
			# if self.loopCount > 1000:
			# 	return False
			print 'searching',len(self.path)
			curr_node = self.open_set.pop(0)
			self.path.append(curr_node)
			self.g_score = self.g_score + 1

			if curr_node == self.goal_node:
				# print 'path in A star',self.path
				self.loopCount = 0
				return self.path

			self.closed_set.append(curr_node)

			neighbors, neighbor_num, obstacles = self.get_neighbors(curr_node)
			print 'curr_node', curr_node
			print 'neighbors', neighbors
			lowest_cost = float('inf')
			for neighbor_node in neighbors:
				print 'for'
				# print 59
				if neighbor_node in self.closed_set:
					print 61
					continue

				tmp_g_score = self.g_score + 1
				tmp_h = self.get_heuristic(neighbor_node)
				tmp_cost = tmp_g_score + tmp_h
				if tmp_cost < lowest_cost:
					lowest_cost = tmp_cost
					closest_node = neighbor_node

			self.open_set.append(closest_node)
			self.loopCount += 1

		return False

	def print_path(self):
		print self.path